package hometask.annotato;

import java.lang.annotation.*;

@Documented
@Target(ElementType.TYPE)
@Inherited
@Retention(RetentionPolicy.RUNTIME)
public @interface MyCustomAnno {
    int yearsOld() default 20;
    String name();
    String workPlace();
}
