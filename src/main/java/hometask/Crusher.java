package hometask;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Random;

public class Crusher {
    private static Logger LOG = LogManager.getLogger(Crusher.class);

    @Test
    public void TestReflectionGeneral(){
        Tester tester = new Tester(new Random().nextInt(50));
        Class cls = tester.getClass();
        LOG.info(cls);
        LOG.info(cls.getFields());
        LOG.info(cls.getMethods());
        LOG.info(cls.getAnnotations());
    }
    @Test
    public void TestReflectionPrivateFields(){
        Tester tester = new Tester(new Random().nextInt(50));
        Class cls = tester.getClass();
        LOG.info("Field before reflection hacking: "+tester.getTestInt());
        try {
            Field myField = cls.getDeclaredField("testInt");
            myField.setAccessible(true);
            LOG.info("Declared annotations for this field: "+ Arrays.toString(myField.getDeclaredAnnotations()));
            try {
                myField.set(tester,new Random().nextInt(50));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            LOG.info("Field after hacking: "+myField.getName()+" = "+tester.getTestInt());
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }
}
