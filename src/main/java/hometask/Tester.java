package hometask;

import hometask.annotato.Fieldy;
import hometask.annotato.MyCustomAnno;

import java.util.Arrays;

@MyCustomAnno(name = "SummerGuy", workPlace = "EPAM")
public class Tester extends Object{
    @Fieldy(author = "Viktorius")
    private int testInt;

    public Tester(int testInt){
        this.testInt=testInt;
    }

    public int getTestInt() {
        return testInt;
    }

    public String myMethod(String param1, String... params){
        return param1 + Arrays.toString(params);
    }
    public String myMethod(String... params){
        return Arrays.toString(params);
    }
}
